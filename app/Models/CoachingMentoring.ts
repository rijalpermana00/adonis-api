import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class CoachingMentoring extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  
  @column()
  public group_id: number
  
  @column()
  public activity_id: number
  
  @column()
  public agenda: string
  
  @column()
  public leader_id: number
  
  @column()
  public employee_id: number
  
  @column()
  public result: string
  
  @column()
  public plan: string
  
  @column()
  public status: number
  
  @column()
  public appointment_time: DateTime
  
  @column()
  public approved_at: DateTime
  
  @column()
  public rejected_at: DateTime

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
