import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CoachingMentoring from 'App/Models/CoachingMentoring';
import ApprovalCoachingMentoringValidator from 'App/Validators/ApprovalCoachingMentoringValidator';
import ConfirmCoachingMentoringValidator from 'App/Validators/ConfirmCoachingMentoringValidator';
import CreateCoachingMentoringValidator from 'App/Validators/CreateCoachingMentoringValidator';
import UpdateCoachingMentoringValidator from 'App/Validators/UpdateCoachingMentoringValidator';
import { DateTime } from 'luxon';

export default class CoachingMentoringsController {
    
    public async create(ctx: HttpContextContract){
        
        try{
            
            const payload = await ctx.request.validate(CreateCoachingMentoringValidator)
            
            const coachingMentoring = await CoachingMentoring.create({
                group_id: payload.groupId,
                activity_id: payload.activityId,
                agenda: payload.agenda,
                leader_id: payload.leaderId,
                employee_id: payload.employeeId,
                appointment_time: payload.appointmentTime,
            })
            
            return {
                success: true,
                message: 'Success',
                data: coachingMentoring
            }
            
        }catch(e){
            
            return {
                success: false,
                message: 'Failed: '+e.message,
                data: null
            }
        }
    }
    
    public async confirm(ctx: HttpContextContract){
        
        try{
            
            const key = ctx.request.params();
            const payload = await ctx.request.validate(ConfirmCoachingMentoringValidator)
            
            const one = await CoachingMentoring.find(key.id);
            
            if(!one){
                return {
                    success: false,
                    info : 'Data is not found',
                    data : null
                }
            }
            
            one.agenda = payload.agenda;
            one.appointment_time = payload.appointmentTime;
            one.status = 1;
            
            one.save();
            
            return {
                success: true,
                message: 'Success',
                data: one
            }
            
        }catch(e){
            
            return {
                success: false,
                message: 'Failed: '+e.message,
                data: null
            }
        }
    }
    
    public async update(ctx: HttpContextContract){
        
        try{
            
            const key = ctx.request.params();
            const payload = await ctx.request.validate(UpdateCoachingMentoringValidator)
            
            const one = await CoachingMentoring.find(key.id);
            
            if(!one){
                return {
                    success: false,
                    info : 'Data is not found',
                    data : null
                }
            }
            
            if(one.status === 3){
                return {
                    status: false,
                    message: 'data is already approved',
                    data: null,
                }
            }
            
            one.agenda = payload.agenda;
            one.result =payload.result;
            one.plan =payload.plan;
            one.status = 2;
            
            one.save();
            
            return {
                success: true,
                message: 'Success',
                data: one
            }
            
        }catch(e){
            
            return {
                success: false,
                message: 'Failed: '+e.message,
                data: null
            }
        }
    }
    
    public async approval(ctx: HttpContextContract){
        
        try{
            
            const key = ctx.request.params();
            const payload = await ctx.request.validate(ApprovalCoachingMentoringValidator)
            
            const one = await CoachingMentoring.find(key.id);
            
            if(!one){
                return {
                    success: false,
                    info : 'Data is not found',
                    data : null
                }
            }
            
            if(one.status === 3){
                return {
                  status: false,
                  message: 'data is already approved',
                  data: null,
                }
            }
              
            if(payload.status === 'APPROVED'){
                one.status = 3;
                one.approved_at = DateTime.local();
            
            }else if(payload.status === 'REJECTED'){
                one.status = 4;
                one.rejected_at = DateTime.local();
            
            }else{
                return {
                status: false,
                message: 'Status is not valid',
                data: null,
                }
            }
            
            one.agenda = payload.agenda ?? one.agenda;
            one.result = payload.result ?? one.result;
            one.plan = payload.plan ?? one.plan;
            
            await one.save();
            
            return {
                success: true,
                message: 'Success',
                data: one
            }
            
        }catch(e){
            
            return {
                success: false,
                message: 'Failed: '+e.message,
                data: null
            }
        }
    }
}
