import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'pfm_coaching_mentoring_adonis'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('group_id')
      table.integer('activity_id')
      table.string('agenda',250).nullable()
      table.integer('leader_id')
      table.integer('employee_id')
      table.string('result',250).nullable()
      table.string('plan',250).nullable()
      table.integer('status')
      table.timestamp('appointment_time').nullable()
      table.timestamp('approved_at').nullable()
      table.timestamp('rejected_at').nullable()
      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
